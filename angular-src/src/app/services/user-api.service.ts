import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import * as ENV from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UserApiService {

  constructor(private http: Http) { }

  /**
   * Method to create the headers for HTTP request
   * @param {Headers} headers Object of type of Headers
   */
  createHeaders(headers: Headers){
    headers.append('Content-Type', 'application/json');
    return headers;
  }

  /**
   * Method making an API call to get the list of Users
   */
  getUsers(): Observable<any>{
    let header = this.createHeaders(new Headers());
    return this.http.get(ENV.environment.apiService+'getUsersFromMongo', {headers: header})
    .pipe(map((data: any) => {
      // console.log("getUsers response: ", data._body);
      return data._body;
    }, err => {
      console.error(err);
      return err;
    }));
  }

  /**
   * Method to make an API call to add the user
   * @param {JSON} user User object
   */
  addUser(user){
    let header = this.createHeaders(new Headers());
    return this.http.post(ENV.environment.apiService+'updateUser', user, {headers: header})
    .pipe(map((data: any) => {
      // console.log("addUser response: ", data._body);
      return true;
    }, err => {
      console.error(err);
      return err;
    }));
  }

  /**
   * Method to make an API call to update user
   * @param {JSON} user User object
   */
  updateUser(user){
    let header = this.createHeaders(new Headers());
    return this.http.put(ENV.environment.apiService+'updateUser', user, {headers: header})
    .pipe(map((data: any) => {
      // console.log("addUser response: ", data._body);
      return true;
    }, err => {
      console.error(err);
      return err;
    }));
  }

  /**
   * Method making an API call to delete the user
   * @param {Number} id User ID 
   */
  deleteUser(id){
    let header = this.createHeaders(new Headers());
    return this.http.delete(ENV.environment.apiService+'deleteUser/'+id, {headers: header})
    .pipe(map((data: any) => {
      // console.log("addUser response: ", data._body);
      return true;
    }, err => {
      console.error(err);
      return err;
    }));
  }
}
