import { Component, OnInit } from '@angular/core';
import { NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import * as io from 'socket.io-client';
import * as ENV from '../../../environments/environment';

import { UserApiService } from '../../services/user-api.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  list: any = [];
  isDataLoaded: boolean = false;
  action: string = '';
  newUser: any = {
    name: null,
    dob: null,
    email: null,
    contact: null
  };
  socket = io(ENV.environment.nodeApi);

  constructor(private userService: UserApiService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getUserList();
    this.socket.on('dataDumped', function(){
      console.log("Socket invoked");
      this.getUserList();
    }.bind(this));
  }

  /**
   * Method to open the Modal for editing details
   * @param {HTML} content HTML content of the modal
   * @param {string} type to identify if it is 'add user' or 'update user' 
   */
  openModal(content, type) {
    this.action = type;
    this.modalService.open(content);
  }

  /**
   * Get the Users List from service
   */
  getUserList() {
    this.userService.getUsers()
      .subscribe((res: any) => {
        res = JSON.parse(res);
        if (res.length > 0) {
          this.isDataLoaded = true;
          this.list = res;
          // console.log(this.list);
        } else {
          this.isDataLoaded = false;
        }
      }, err => {
        this.isDataLoaded = false;
        console.log(err);
      });
  }

  /**
   * Method to add user
   */
  addUser() {
    // console.log("New User:" + JSON.stringify(this.newUser));
    this.userService.addUser(this.newUser)
      .subscribe(res => {
        // console.log(res);
        this.getUserList();
        this.resetNewUser();
      })
  }

  /**
   * Method to edit the user
   * @param {JSON} user User object
   * @param {HTML} content HTML content of the modal 
   * @param {String} type to identify if it is 'add user' or 'update user'
   */
  editUser(user, content, type) {
    this.newUser = {
      id: user.id,
      name: user.name,
      dob: user.dob,
      email: user.email,
      contact: user.contact
    };
    this.openModal(content, type);
  }

  /**
   * Method to update the user in the service
   */
  updateUser() {
    // console.log('in updateUser:' + JSON.stringify(this.newUser));
    this.userService.updateUser(this.newUser)
      .subscribe(res => {
        this.resetNewUser();
        // console.log(res);
      })
  }

  /**
   * Method to delete the user in the service
   * @param {JSON} user User object 
   */
  deleteUser(user) {
    // console.log('in deleteUser');
    this.userService.deleteUser(user.id)
      .subscribe(res => {
        // console.log(res);
      })
  }

  /**
   * Method called on submitting the user details on the modal
   */
  submit() {
    if (this.action === 'new') {
      this.addUser();
    } else {
      this.updateUser();
    }
  }

  /**
   * Method to reset the New User object
   */
  resetNewUser() {
    this.newUser.name = null;
    this.newUser.dob = null;
    this.newUser.email = null;
    this.newUser.contact = null;
  }

}
