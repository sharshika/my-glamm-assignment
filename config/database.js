module.exports = {
    mongoUrl: "mongodb://localhost:27017/myGlammUserDB",
    
    //MySQL configuration
    mysqlUser: 'root',
    mysqlPassword: '',
    mysqlDB: 'test',
    tableName: 'UsersTestDB',

    createTableQuery: 'CREATE TABLE UsersTestDB(id INT PRIMARY KEY AUTO_INCREMENT,name VARCHAR(50), dob DATE, email VARCHAR(100), contact BIGINT)'
};